﻿using UnityEngine;
using System.Collections;

public struct nodePosition {

	public int x;
	public int y;

	public nodePosition (int _x, int _y) {

		x = _x;
		y = _y;

	}

}
