﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class main : MonoBehaviour {

	public Vector2 size;

	public Transform nodeTransform;

	private Transform thisTransform;

	public gridNodeBehaviour[,] grid;

	private List<gridNodeBehaviour> selectedNodes;

	private List<gridNodeBehaviour> oldRoute;

	public Text directText;
	public Text shortestText;

	// Use this for initialization
	void Start () {

		thisTransform = transform;

		grid = 	new	gridNodeBehaviour[(int)size.x, (int)size.y];

		selectedNodes = new List<gridNodeBehaviour> ();
		oldRoute = new List<gridNodeBehaviour> ();

		makeGrid ();

	}

	void makeGrid () {

		//create gridnodes
		Vector3 pos = Vector3.zero;
		for (int x = 0; x < size.x; x++) {
			for (int y = 0; y < size.y; y++) {

				pos.x = x - (size.x /2); pos.y = y - (size.y /2);

				Transform nodeInstance = Instantiate(nodeTransform, pos, Quaternion.identity) as Transform;

				nodeInstance.parent = thisTransform;
				nodeInstance.name = x + " - " + y + " node";

				gridNodeBehaviour node = nodeInstance.GetComponent<gridNodeBehaviour>();

				//set stats
				node.position = new nodePosition(x,y);
				node.height = Random.Range(0,127);
			

				//set access to this script
				node.grid = this;

				grid[x,y] = node;
			}
		}

		//go through nodes and give them connections to their neighboards. Needed for dijikstra algorithm
		for (int x = 0; x < size.x; x++) {
			for (int y = 0; y < size.y; y++) {

				if (x > 0) {
					addConnectionBetween (grid[x,y], grid[x-1,y]);
				}
				if (x < size.x-1) {
					addConnectionBetween (grid[x,y], grid[x+1,y]);
				}

				if (y > 0) {
					addConnectionBetween (grid[x,y], grid[x,y-1]);
				}
				if (y < size.y-1) {
					addConnectionBetween (grid[x,y], grid[x,y+1]);
				}
			}
		}

	}

	public void addNodeToSelected(gridNodeBehaviour node) {
	//	gridNodeBehaviour node = grid [(int)_position.x, (int)_position.y];

		node.activateNode ();

		selectedNodes.Add (node);

		//keep selected nodes at 2
		if (selectedNodes.Count > 2) {
//			Debug.Log("disables " + selectedNodes[0].position);
			selectedNodes[0].disableNode();
			selectedNodes.RemoveAt(0);
		}

		if (selectedNodes.Count == 2) {

			calculateDirectRoute (selectedNodes[0], selectedNodes[1]);

			calculateShortestRoute (selectedNodes[0], selectedNodes[1]);


		}

	}
	
	private void calculateShortestRoute(gridNodeBehaviour _start, gridNodeBehaviour _end) {

		//reset previous route 
		resetGrid ();
				
		//made temporary one dimensional list of all nodes, this will optimize searching closest node
		//since nodes can be removed from the list once used
		List<gridNodeBehaviour> temp = makeTempList(grid);

		//clear markings from previous route
		clearOldMarkings ();

		nodePosition start = _start.position;
		nodePosition end = _end.position;		
	
		//set starting position to distance 0
		int x = _start.position.x; int y = _start.position.y;
		grid [x, y].distanceToSource = 0;

		//continue this until route to target is found, or route between spots cant be found
		while (1 != 2) {

			//find node with shortest distance to source
			//gridNodeBehaviour closestNode = searchSmallestDistance(grid);
			gridNodeBehaviour closestNode = searchSmallestDistanceFromList(temp);

			if (closestNode == null) break; // end loop if there is no more nodes to find

			//check closest node as visited
			closestNode.isVisited = true;

			//check all connections from closest node and calculate/update distances to its neighboars
			foreach (var c in closestNode.connections) {
				nodePosition pos = c.Key;

				float distance = closestNode.distanceToSource + c.Value;

				if (grid[pos.x, pos.y].distanceToSource > distance) {
					grid[pos.x, pos.y].distanceToSource = distance;
					grid[pos.x, pos.y].previousNode = closestNode;
				}

			}

			//if closestnode is same as end, route has been found
			if (closestNode.position.x == end.x && closestNode.position.y == end.y) break;

		}
	//	Debug.Log (temp.Count);

		//route has been found, go throught it in reverse order and mark the nodes
		gridNodeBehaviour node = _end;
		while (1 != 2) {
			node.markNode();
			oldRoute.Add(node);
			node = node.previousNode;
			if (node.position.x == start.x && node.position.y == start.y) break;
		}

		//Debug.Log ("smallest distance: " + _end.distanceToSource);
		shortestText.text = _end.distanceToSource + "";

	}

	private List<gridNodeBehaviour> makeTempList(gridNodeBehaviour[,] tempGrid) {

		List<gridNodeBehaviour> tempList = new List<gridNodeBehaviour>();

		for (int x = 0; x < size.x; x++) {
			for (int y = 0; y < size.y; y++) {
				tempList.Add(tempGrid[x,y]);
			}
		}

		return tempList;

	}

	private void clearOldMarkings() {
		//clear markings from previous route
		foreach (gridNodeBehaviour node in oldRoute) {
			node.unMarkNode();
		}
		oldRoute.Clear();
	}

	private void resetGrid() {
		for (int x = 0; x < size.x; x++) {
			for (int y = 0; y < size.y; y++) {
				grid[x,y].isVisited = false;
				grid[x,y].distanceToSource = float.PositiveInfinity;
				grid[x,y].previousNode = null;
			}			
		}
	}

	//search node with smallest value in distanceToSource
	private gridNodeBehaviour searchSmallestDistance(gridNodeBehaviour[,] tempGrid) {
		float smallestDistance = float.PositiveInfinity;
		gridNodeBehaviour nodeWithSmallestDistance = null;



		for (int x = 0; x < size.x; x++) {
			for (int y = 0; y < size.y; y++) {

				if (tempGrid[x,y].isVisited == false) { //only check nodes that are not checked before

					if (tempGrid[x,y].distanceToSource < smallestDistance) {

						nodeWithSmallestDistance = tempGrid[x,y];
						smallestDistance = tempGrid[x,y].distanceToSource;

						if (smallestDistance == 0) break;
					}
				}
			}

		}

		//return null if this either does not find non visited node or node which has some non-infinite distance to source
		if (smallestDistance == float.PositiveInfinity)	return null;

		return nodeWithSmallestDistance;

	}

	//experimental function to search smallest node from list
	private gridNodeBehaviour searchSmallestDistanceFromList (List<gridNodeBehaviour> temp) {

		float smallestDistance = float.PositiveInfinity;
		gridNodeBehaviour nodeWithSmallestDistance = null;

		int index = 0;
		int smallestIndex = -1;

		foreach (gridNodeBehaviour g in temp) {

			if (g.distanceToSource < smallestDistance) {
				
				nodeWithSmallestDistance = g;
				smallestDistance = g.distanceToSource;
				smallestIndex = index;

				if (smallestDistance == 0) break;
			}

			index++;

		}

		//return null if this either does not find non visited node or node which has some non-infinite distance to source
		if (smallestDistance == float.PositiveInfinity)	return null;

		if (smallestIndex != -1) temp.RemoveAt (smallestIndex);

		return nodeWithSmallestDistance;


	}


	private void calculateDirectRoute(gridNodeBehaviour _start, gridNodeBehaviour _end) {

		int sx = _start.position.x;
		int sy = _start.position.y;
		int ex = _end.position.x;
		int ey = _end.position.y;

		int x = sx; int y = sy;

		//clear markings from previous route
	/*	foreach (gridNodeBehaviour node in oldRoute) {
			node.unMarkNode();
		}
		oldRoute.Clear();*/

		float totalLength = 0;

		while (x != ex || y != ey) {

			if (x < ex) {
				totalLength += grid[x,y].connections[grid[x+1,y].position];
				x++;
			}
			if (x > ex) {
				totalLength += grid[x,y].connections[grid[x-1,y].position];
				x--;

			}
			grid[x,y].markNode();
			oldRoute.Add(grid[x,y]);

			if (y < ey) {
				totalLength += grid[x,y].connections[grid[x,y+1].position];
				y++;
			}
			if (y > ey) {
				totalLength += grid[x,y].connections[grid[x,y-1].position];
				y--;
				
			}
		//	grid[x,y].markNode();
		//	oldRoute.Add(grid[x,y]);

		}
		//Debug.Log ("direct route: " + totalLength);
		directText.text = totalLength + "";

		//TODO dijk 

		//TODO vanhojen routejen disablointi? ehkä helpointa tehdä eventti jonka kaikki pallerot näkee?
	}

	void addConnectionBetween(gridNodeBehaviour _start, gridNodeBehaviour _end) {

		if (_start && _end) {
		//	gridConnection gc;
			float distance;
			
			//calculate distance 
			distance = _end.height - _start.height;
			if (distance < 0) distance = 0; //downhill is zero
		//	gc.target = _end.position;
		//	gc.distance = distance;
			
			//_start.connections.Add(gc);

			_start.connections.Add(_end.position, distance);
		}
	}
	
	// Update is called once per frame
	/*void Update () {
	
	}*/
}
