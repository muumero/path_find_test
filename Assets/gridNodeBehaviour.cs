﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class gridNodeBehaviour : MonoBehaviour {

	[HideInInspector]
	//public Vector2 position;
	public nodePosition position; 

	//[HideInInspector]
	public float height;

	//[HideInInspector]
//	public List<gridConnection> connections;
	public Dictionary<nodePosition, float> connections;

	private MeshRenderer renderer;

	public Color activeColor;
	public Color inActiveColor;
	public Color routeMarkerColor;

	[HideInInspector]
	public main grid;

	private bool isActivated = false;

	//for path finding
	[HideInInspector]
	public bool isVisited = false;
//	[HideInInspector]
	//public bool allPathsChecked = false;
	[HideInInspector]
	public gridNodeBehaviour previousNode; //this allows backtracking path 
	[HideInInspector]
	public float distanceToSource = float.PositiveInfinity;

	void Awake() {

		connections = new Dictionary<nodePosition, float> ();
	//	connections = new List<gridConnection> (); 

		renderer = GetComponent<MeshRenderer>();
		disableNode ();
	}

	void OnMouseDown() {
	//	Debug.Log ("clicked " + position); 
		grid.addNodeToSelected (this);
	//	transform.parent.GetComponent<createGrid> ().addNodeToSelected (position);
	}

	public void activateNode() {
		renderer.material.color = activeColor;
		isActivated = true;
	}

	public void markNode() {
		if (!isActivated) renderer.material.color = routeMarkerColor;
	}

	public void unMarkNode() {
		if (!isActivated) renderer.material.color = inActiveColor;
	}

	public void disableNode() {
		renderer.material.color = inActiveColor;
		isActivated = false;
	}


}
